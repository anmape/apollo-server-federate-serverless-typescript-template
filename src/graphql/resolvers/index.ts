import mergeDeep from 'graphql-tools/dist/mergeDeep';

import defaultResolver from './default.resolver';

let resolvers: {} = {};

resolvers = mergeDeep(resolvers, defaultResolver);

export default resolvers;
